# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum
* Key functions (add/delete)
    1. user page
    2. post page
    3. post list page
    4. leave comment under any post
* Other functions (add/delete)
    1. Sign Up/In with Google accounts
    2. Use CSS animation
    3. 在user page可以修改個人資料（暱稱、關於我）
    4. 在user page可以上傳頭像（使用Storage存圖片）
    5. 在文章中會顯示發文者的頭像及暱稱
    6. 點選左上logo可以返回首頁 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y| (Firebase Page)
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description

Basic Components：
* Membership Mechanism
    可用email或google帳號登入
    每個會員有個人頁面、可以修改個人資料（暱稱、關於我）、可以上傳頭像
* Firebase Page
    網址：https://midterm-105060001.firebaseapp.com/index.html
* Database
    存文章及會員資料
* RWD
    有實作RWD
* Topic Key Function
    會員頁面、文章列表、點選列表中的文章進入文章頁面、可在每篇文章頁面下留言

Advanced Components：
* Third-Party Sign In
    可用google帳號登入
* Use CSS Animation
    index.html中的banner會變色

額外：
* 在user page可以修改個人資料（暱稱、關於我）
* 在user page可以上傳頭像（使用Storage存圖片）
* 在文章中會顯示發文者的頭像及暱稱
* 點選左上logo可以返回首頁

## Security Report (Optional)
