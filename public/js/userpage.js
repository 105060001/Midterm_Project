function init() {

    pro_pic = document.getElementById('pro_pic');
    pro_name = document.getElementById('pro_name');
    pro_email = document.getElementById('pro_email');
    pro_about = document.getElementById('pro_about');

    var loginUser;
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var post_btn = document.getElementById('post-btn');
        if (user) {
            /////
            firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot) {
                pro_name.innerHTML=snapshot.val().name;
                pro_email.innerHTML=snapshot.val().email;
                pro_about.innerHTML=snapshot.val().about;
            //    console.log(snapshot.val().name);
            });
            var storageRef = firebase.storage().ref();

            storageRef.child(user.uid).getDownloadURL().then(function (url) {
                document.getElementById('pro_pic').src = url;
            }).catch(function (error) {
                console.log(error.message);
            });
            /////
            loginUser=user;
            user_email = user.email;
            menu.innerHTML = "<a href='userpage.html' class='dropdown-item'>" + user.email + "</a><span class='dropdown-item' id='logout-btn'>登出</span>";
            post_btn.innerHTML='<a class="nav-link" href="post.html">發文</a>';
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('登出成功')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>登入</a>";
            post_btn.innerHTML='';
            //document.getElementById('post_list').innerHTML = "";
        }
    });
    
/*
    pro_change_btn = document.getElementById('pro_change_btn');

    pro_change_btn.addEventListener('click', function () {
        location.href("chageprofile.html");
    });
*/
}

window.onload = function () {
    init();
}