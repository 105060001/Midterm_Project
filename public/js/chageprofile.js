function init() {

    var loginUser;
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var post_btn = document.getElementById('post-btn');
        if (user) {
            /////顯示原本的
            firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot) {
                document.getElementById('chaged_name').value=snapshot.val().name;
                document.getElementById('chaged_about').value=snapshot.val().about;
            });
            var storageRef = firebase.storage().ref();
            storageRef.child(user.uid).getDownloadURL().then(function (url) {
                document.getElementById('view_pic').src = url;
            }).catch(function (error) {
                console.log(error.message);
            });
            /////

            loginUser = user;
            user_email = user.email;
            menu.innerHTML = "<a href='userpage.html' class='dropdown-item'>" + user.email + "</a><span class='dropdown-item' id='logout-btn'>登出</span>";
            post_btn.innerHTML='<a class="nav-link" href="post.html">發文</a>';
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('登出成功')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>登入</a>";
            post_btn.innerHTML='';
            //document.getElementById('post_list').innerHTML = "";
        }
    });

    submit_btn = document.getElementById('submit_btn');
    chaged_name = document.getElementById('chaged_name');
    chaged_about = document.getElementById('chaged_about');
    view_pic = document.getElementById('view_pic');
    //menu.innerHTML ='<img src="./img/addImg.png" height="20" width="20">'

    ///上傳圖片
    var picInput = document.getElementById("chaged_pic");
    var storageRef = firebase.storage().ref();
    var file;
/*
    storageRef.child(loginUser.uid).getDownloadURL().then(function(URL){
        document.getElementById('view_pic').src = URL;
    })
*/
    picInput.addEventListener("change", function(){

        file = this.files[0];
        document.getElementById('view_pic').src = URL.createObjectURL(file);


       })

    submit_btn.addEventListener('click', function () {
        ////改資料
        firebase.database().ref('/users/' + loginUser.uid).update({
            name: chaged_name.value,
            about: chaged_about.value,
            //  photoURL: downloadURL
        });
        ////改頭像
        if(file){
            storageRef.child(loginUser.uid).put(file).then(function (snapshot) {
                document.getElementById('view_pic').src = URL.createObjectURL(file);
                })
                .catch(function (error) {
                    console.log(error);
            });
        }


        alert('更改成功');

    });

 }

window.onload = function () {
            init();
        }