function init() {

    var loginUser;
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var post_btn  = document.getElementById('post-btn');
        if (user) {
            loginUser=user;
            user_email = user.email;
            menu.innerHTML = "<a href='userpage.html' class='dropdown-item'>" + user.email + "</a><span class='dropdown-item' id='logout-btn'>登出</span>";
            post_btn.innerHTML='<a class="nav-link" href="post.html">發文</a>';
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('登出成功')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>登入</a>";
            post_btn.innerHTML='';
            //document.getElementById('post_list').innerHTML = "";
        }
    });



    var str_before_username = 
    "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src='img/star.png' alt='' class='mr-2 rounded' style='height:24px; width:24px;'><p class='media-body pb-3 mb-0 lh-125 border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref('posts');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();

                total_post[total_post.length] = 
                "<a href='thread.html?tid="+childSnapshot.key+ "'>"+
                str_before_username + "["+childData.class+"] "+ childData.title
                + "</strong>" + str_after_content + "</a>";

                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    
                    total_post[total_post.length] = 
                    "<a href='thread.html?tid="+childSnapshot.key+ "'>"+
                    str_before_username + "["+childData.class+"]"+ childData.title
                    + "</strong>" + str_after_content + "</a>";

                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
}